package cn.afterturn.easypoi.zzgdemo.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import cn.afterturn.easypoi.zzgdemo.utils.DateUtils;
import lombok.Data;
import org.apache.commons.lang3.RandomUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @Author 张志刚
 * @Date 2021/9/19
 * @Description
 */
@Data
public class StudentModel implements java.io.Serializable {

    // ID
    private String        id;

    // 学生姓名
    @Excel(name = "学生姓名", height = 20, width = 30, mergeVertical = true)
    private String        name;

    // 学生性别
    @Excel(name = "学生性别", replace = { "男_1", "女_2" }, suffix = "生", isImportField = "true_st")
    private int           sex;

    @Excel(name = "出生日期", databaseFormat = "yyyyMMddHHmmss", format = "yyyy-MM-dd", isImportField = "true_st", width = 20)
    private Date birthday;

    @Excel(name = "进校日期", databaseFormat = "yyyyMMddHHmmss", format = "yyyy-MM-dd", width = 20)
    private Date registrationDate;



    public static List<StudentModel> getStudentList(int size) {
        String names = "张立国,张国华,刘德华,张学友,奥斯特洛夫斯基,孔子,苏格拉底苏格拉底苏格拉底苏格拉底苏格拉底苏格拉底苏格拉底苏格拉底";
        List<String> nameList = Arrays.asList(names.split(","));

        List<StudentModel> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            StudentModel student = new StudentModel();
            student.setId(String.valueOf(i));
            String randomName = nameList.get(RandomUtils.nextInt(0, nameList.size()));
            student.setName(randomName);

            student.setSex(RandomUtils.nextInt(1, 3));

            Date randomBir = new Date(System.currentTimeMillis() - RandomUtils.nextLong(DateUtils.DAY * 1000L, DateUtils.DAY * 10000L));
            student.setBirthday(randomBir);

            student.setRegistrationDate(randomBir);
            list.add(student);
        }
        return list;
    }


}
