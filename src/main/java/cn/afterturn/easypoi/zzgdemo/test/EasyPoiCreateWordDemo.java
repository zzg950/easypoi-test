package cn.afterturn.easypoi.zzgdemo.test;

import cn.afterturn.easypoi.entity.ImageEntity;
import cn.afterturn.easypoi.word.WordExportUtil;
import cn.afterturn.easypoi.zzgdemo.utils.DateUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author 张志刚
 * @Date 2021/10/20
 * @Description TODO
 *
 * http://easypoi.mydoc.io/#text_226102
 */
public class EasyPoiCreateWordDemo {

    public static void main(String[] args) throws Exception {
//        EasyPoiCreateWordDemo.exportLocalFile();
//        EasyPoiCreateWordDemo.exportLocalMorePageFile();
        EasyPoiCreateWordDemo.createWordByTemplate();
    }

    public static void exportLocalFile() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("title", "这是一个大标题");
        map.put("person", "JueYue");
        map.put("time", DateUtils.getDatetime());

        XWPFDocument workDoc = WordExportUtil.exportWord07("word/simpleTemplate.docx", map);
        FileOutputStream fos = new FileOutputStream("D:/simpleTemplate-result.docx");
        workDoc.write(fos);
        fos.close();
    }

    public static void exportLocalMorePageFile() throws Exception {
        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("title", "标题" + i);
            map.put("name", "姓名" + i);
            map.put("birthday", DateUtils.getDate());
            list.add(map);
        }

        XWPFDocument workDoc = WordExportUtil.exportWord07("word/morePageTemplate.docx", list);
        FileOutputStream fos = new FileOutputStream("D:/morePageTemplate-result.docx");
        workDoc.write(fos);
        fos.close();
    }

    public static void createWordByTemplate() throws Exception {
        String templatePath = "word/byzs_template.docx";
        String targetPath = "D:/byzs_template.docx";

        Map<String, Object> map = new HashMap<>();
        map.put("XM", "张志刚");
        map.put("XB", "男");
        map.put("SR", DateUtils.getDatetime("yyyy年MM月dd日"));

        /**
         * 图片
         */
        String picUrl = "http://mini37.com/source/mini37/gamepic/160x210/1505815776206..jpg";
        ImageEntity image = new ImageEntity();
        image.setHeight(100);
        image.setWidth(150);
        image.setUrl(picUrl);

        /*WordImageEntity image = new WordImageEntity();
        image.setHeight(200);
        image.setWidth(500);
        image.setUrl("cn/afterturn/easypoi/test/word/img/testCode.png");
        image.setType(WordImageEntity.URL);*/
        map.put("HEADERIMG", image);

        XWPFDocument workDoc = WordExportUtil.exportWord07(templatePath, map);
        FileOutputStream fos = new FileOutputStream(targetPath);
        workDoc.write(fos);
        fos.close();
    }
}
