package cn.afterturn.easypoi.zzgdemo.test;


import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfDocumentInfo;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @Author 张志刚
 * @Date 2021/9/22
 * @Description 依赖 itext 创建PDF
 */
public class CreatePdfDemo {

    public static void main(String[] args) throws IOException {
        File pdfFile = new File("D:\\home\\itext.pdf");
        PdfWriter pdfWriter=new PdfWriter(pdfFile);

        PdfDocument pdfDocument = new PdfDocument(pdfWriter);

        Document document=new Document(pdfDocument);
        PdfDocumentInfo documentInfo=pdfDocument.getDocumentInfo();
        documentInfo.setCreator("zxj");

        Paragraph paragraph=new Paragraph("good morning~~~早上好");
        PdfFont font = PdfFontFactory.createFont("STSongStd-Light","UniGB-UCS2-H",true);
        paragraph.setFont(font);

        document.add(paragraph);

        document.close();

        System.out.println("ok!!!");

    }


}
