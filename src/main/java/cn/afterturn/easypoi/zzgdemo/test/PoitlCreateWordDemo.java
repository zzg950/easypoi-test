package cn.afterturn.easypoi.zzgdemo.test;

import org.apache.poi.xwpf.usermodel.*;

import java.io.FileInputStream;
import java.util.List;

/**
 * @Author 张志刚
 * @Date 2021/11/18
 * @Description TODO
 * POI-TL 使用文档 : http://deepoove.com/poi-tl/#_maven
 * 通过
 * http://deepoove.com/poi-tl/apache-poi-guide.html
 * https://www.cnblogs.com/gscq073240/articles/11477812.html
 */
public class PoitlCreateWordDemo {

    public static void main(String[] args) throws Exception {
        String filename = "崔吟雪成绩单 (1).docx";
        String wordPath = "D:\\home\\word\\" + filename;

        FileInputStream fis = new FileInputStream(wordPath);
        XWPFDocument doc = new XWPFDocument(fis);

        // 段落
        List<XWPFParagraph> paragraphs = doc.getParagraphs();
        // 表格
        List<XWPFTable> tables = doc.getTables();
        // 图片
        List<XWPFPictureData> allPictures = doc.getAllPictures();
        // 页眉
        List<XWPFHeader> headerList = doc.getHeaderList();
        // 页脚
        List<XWPFFooter> footerList = doc.getFooterList();
    }

}
