package cn.afterturn.easypoi.zzgdemo.test;

import cn.afterturn.easypoi.zzgdemo.utils.PdfUtils;

/**
 * @Author 张志刚
 * @Date 2021/9/24
 * @Description TODO
 */
public class AsposeExcelToPdfDemo {

    public static void main(String[] args) {
        String filename = "exportTemp12121212.xlsx";
        String sourceFilePath="d:/home/excel/" + filename;
        String desFilePath="d:/home/excel/" + filename + ".pdf";
        PdfUtils.excel2pdf(sourceFilePath, desFilePath);
    }

}
