package cn.afterturn.easypoi.zzgdemo.test;

import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;

import java.io.File;

/**
 * @Author 张志刚
 * @Date 2021/11/19
 * @Description 合并PDF
 *
 * https://blog.csdn.net/spring_is_coming/article/details/109390349
 */
public class MegerPdfDemo {

    public static void main(String[] args) throws Exception {

        File file = new File("D:\\home\\word\\崔吟雪成绩单 (1).docx.pdf");
        File file1 = new File("D:\\home\\word\\崔吟雪成绩单 (1).docx-121212.pdf");

        PDFMergerUtility mergePdf = new PDFMergerUtility();
        mergePdf.addSource(file);
        mergePdf.addSource(file1);

        // 设置合并生成pdf文件名称及路径
        String targetPath = "D:\\home\\word\\崔吟雪成绩单-megered.docx.pdf";
        mergePdf.setDestinationFileName(targetPath);
        // 合并pdf
        mergePdf.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());
    }

}
