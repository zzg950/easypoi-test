package cn.afterturn.easypoi.zzgdemo.test;

import cn.afterturn.easypoi.pdf.PdfExportUtil;
import cn.afterturn.easypoi.pdf.entity.PdfExportParams;
import cn.afterturn.easypoi.zzgdemo.model.StudentModel;
import com.itextpdf.layout.Document;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

/**
 * @Author 张志刚
 * @Date 2021/9/19
 * @Description TODO
 */
public class PdfExportDemo {

    public static void main(String[] args) throws Exception{
        PdfExportDemo.exportLocalFile();
    }

    public static void exportLocalFile() throws Exception {
        List<StudentModel> list = StudentModel.getStudentList(10);

        PdfExportParams params = new PdfExportParams("这是一个一级标题","");
        File file = new File("D:/home/excel/result.pdf");
        file.createNewFile();
        Document document =  PdfExportUtil.exportPdf(params, StudentModel.class, list, new FileOutputStream(file));

    }

    public static void exportPdf() {
    }

}
