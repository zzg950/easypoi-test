package cn.afterturn.easypoi.zzgdemo.test;

import cn.afterturn.easypoi.zzgdemo.utils.PdfUtils;
import com.aspose.words.Document;
import com.aspose.words.HeaderFooter;
import com.aspose.words.HeaderFooterType;
import com.aspose.words.Section;

/**
 * @Author 张志刚
 * @Date 2021/9/24
 * @Description TODO
 *
 * 增加水印
 * https://www.cnblogs.com/name-lizonglin/p/12836451.html
 *
 */
public class AsposeWordToPdfDemo {

    public static void main(String[] args) {
        String filename = "崔吟雪成绩单 (1).docx";
        String wordPath = "D:\\home\\word\\" + filename;
        String pdfPath = "D:\\home\\word\\" + filename + ".pdf";
//        PdfUtils.word2pdf(wordPath, pdfPath);
        PdfUtils.word2pdfV2(wordPath, pdfPath);
        /*try {
            long old = System.currentTimeMillis();
            File file = new File(pdfPath); //新建一个pdf文档
            FileOutputStream os = new FileOutputStream(file);
            Document doc = new Document(wordPath); //Address是将要被转化的word文档
            removeWatermark(doc);
            doc.save(os, com.aspose.words.SaveFormat.PDF);//全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
            long now = System.currentTimeMillis();
            os.close();
            System.out.println("共耗时：" + ((now - old) / 1000.0) + "秒"); //转化用时
        } catch (Exception e) {
            e.printStackTrace();
        }
        */
    }

    /**
     * 移除全部水印
     * @param doc
     * @throws Exception
     */
    private static void removeWatermark(Document doc) throws Exception {
        for (Section sect : doc.getSections()) {
            // There could be up to three different headers in each section, since we want
            // the watermark to appear on all pages, insert into all headers.
            removeWatermarkFromHeader(sect, HeaderFooterType.HEADER_PRIMARY);
            removeWatermarkFromHeader(sect, HeaderFooterType.HEADER_FIRST);
            removeWatermarkFromHeader(sect, HeaderFooterType.HEADER_EVEN);
        }
    }

    /**
     * 移除指定Section的水印
     * @param sect
     * @param headerType
     * @throws Exception
     */
    private static void removeWatermarkFromHeader(Section sect, int headerType) throws Exception {
        HeaderFooter header = sect.getHeadersFooters().getByHeaderFooterType(headerType);
        if (header != null) {
            header.removeAllChildren();
        }
    }

}
