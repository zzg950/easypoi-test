package cn.afterturn.easypoi.zzgdemo.test;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.TemplateExportParams;
import cn.afterturn.easypoi.zzgdemo.model.StudentModel;
import cn.afterturn.easypoi.zzgdemo.utils.PdfUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author 张志刚
 * @Date 2021/9/19
 * @Description TODO
 */
public class ExcelExportDemo {

    public static void main(String[] args) throws Exception {
//        ExcelExportDemo.exportLocalFile();
        ExcelExportDemo.exportExcelByTemplate();
    }

    /**
     * 根据实体注解来导出Excel
     * @throws IOException
     */
    public static void exportLocalFile() throws IOException {
        List<StudentModel> list = StudentModel.getStudentList(1000);
        ExportParams params = new ExportParams();
        params.setTitle("计算机一班学生");
        params.setHeaderColor(HSSFColor.HSSFColorPredefined.RED.getIndex());
        params.setHeaderHeight(50);
        params.setSheetName("学生信息");

        Workbook workbook = ExcelExportUtil.exportExcel(params, StudentModel.class, list);
        FileOutputStream fos = new FileOutputStream("D:/ExcelExportHasImgTest.exportCompanyImg.xls");
        workbook.write(fos);

        fos = new FileOutputStream("D:/ExcelExportHasImgTest.exportCompanyImg.xlsx");
        workbook.write(fos);
        fos.close();
    }

    public static void exportExcelByTemplate() throws Exception {
        File file = new File("D:\\workspace\\easypoi-test\\src\\main\\resources\\excel\\template_pdf.xlsx");
        System.out.println(file.exists());
        TemplateExportParams params = new TemplateExportParams("D:\\workspace\\easypoi-test\\src\\main\\resources\\excel\\exportTemp.xls", true);

        params.setHeadingRows(2);
        params.setHeadingStartRow(2);

        Map<String, Object> map = new HashMap<>();
        map.put("year", "2013");
        map.put("sunCourses", "中国人民解放军中华人民共和国");
        map.put("month", 10);
        map.put("tname", "奥斯特洛夫斯基钢铁是怎样炼成的");

        String filename = "exportTemp12121212.xlsx";
        String sourceFilePath = "D:/home/excel/" + filename;
        Workbook book = ExcelExportUtil.exportExcel(params, map);
        FileOutputStream fos = new FileOutputStream(sourceFilePath);
        book.write(fos);
        fos.close();

        String desFilePath = sourceFilePath + ".pdf";
        PdfUtils.excel2pdf(sourceFilePath, desFilePath);

    }

}
