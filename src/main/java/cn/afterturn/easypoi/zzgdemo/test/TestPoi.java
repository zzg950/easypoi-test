package cn.afterturn.easypoi.zzgdemo.test;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

import com.aspose.words.Document;
import com.aspose.words.License;
import com.aspose.words.SaveFormat;

public class TestPoi {

	public static void main(String[] args) {
		doc2pdf("D:\\home\\word\\拟录取推免生汇总表（普通推免）20210923100702.docx","D:\\home\\word\\testDoc121212121212.pdf");
	}
	
	private static boolean getWordLicense() {
        boolean result = false;
        try {
            // 凭证
            String license =
            "<License>\n" +
                    "  <Data>\n" +
                    "    <Products>\n" +
                    "      <Product>Aspose.Total for Java</Product>\n" +
                    "      <Product>Aspose.Words for Java</Product>\n" +
                    "    </Products>\n" +
                    "    <EditionType>Enterprise</EditionType>\n" +
                    "    <SubscriptionExpiry>20991231</SubscriptionExpiry>\n" +
                    "    <LicenseExpiry>20991231</LicenseExpiry>\n" +
                    "    <SerialNumber>8bfe198c-7f0c-4ef8-8ff0-acc3237bf0d7</SerialNumber>\n" +
                    "  </Data>\n" +
                    "  <Signature>sNLLKGMUdF0r8O1kKilWAGdgfs2BvJb/2Xp8p5iuDVfZXmhppo+d0Ran1P9TKdjV4ABwAgKXxJ3jcQTqE/2IRfqwnPf8itN8aFZlV3TJPYeD3yWE7IT55Gz6EijUpC7aKeoohTb4w2fpox58wWoF3SNp6sK6jDfiAUGEHYJ9pjU=</Signature>\n" +
                    "</License>";
            InputStream is = new ByteArrayInputStream(license.getBytes("UTF-8"));
            License asposeLic = new License();
            asposeLic.setLicense(is);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
	
	public static void doc2pdf(String docPath,String pdfPath) {
        if (!getWordLicense()) {
            return;
        }

        try {
            Document convertDoc = new Document(new FileInputStream(docPath));
            convertDoc.save(pdfPath, SaveFormat.PDF);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   
}
