package cn.afterturn.easypoi.zzgdemo.test;

import cn.afterturn.easypoi.zzgdemo.utils.DateUtils;
import cn.afterturn.easypoi.zzgdemo.utils.PdfUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @Author 张志刚
 * @Date 2021/9/22
 * @Description TODO
 */
public class CreateExcelDemo {

    public static void main(String[] args) throws IOException {
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet=wb.createSheet("拟录取推免生汇总表");
        HSSFRow row1=sheet.createRow(0);
        HSSFCell cell=row1.createCell(0);
        sheet.addMergedRegion(new CellRangeAddress(0,0,0,17));
        cell.setCellValue("拟录取推免生汇总表");
        cell.setCellStyle(Style2(wb,"BOLD","CENTER",""));
        row1.setHeight((short)300);

        //在sheet里创建第二行
        HSSFRow row2 = sheet.createRow(1);
        HSSFCell cell2 = row2.createCell(0);
        //创建单元格并设置单元格内容
        sheet.addMergedRegion(new CellRangeAddress(1,1,0,17));
        cell2.setCellValue("日期： " + DateUtils.getDatetime("yyyy年MM月dd日"));
        cell2.setCellStyle(Style2(wb,"BOLD","RIGHT",""));

        //在sheet里创建第三行
        HSSFRow row3=sheet.createRow(2);
        //创建单元格并设置单元格内容
        sheet.addMergedRegion(new CellRangeAddress(2,3,0,0));
        row3.createCell(0).setCellValue("序号");


        sheet.addMergedRegion(new CellRangeAddress(2,3,1,1));
        row3.createCell(1).setCellValue("拟录取专业");

        sheet.addMergedRegion(new CellRangeAddress(2,3,2,2));
        row3.createCell(2).setCellValue("拟录取方向");

        sheet.addMergedRegion(new CellRangeAddress(2,3,3,3));
        row3.createCell(3).setCellValue("姓名");

        sheet.addMergedRegion(new CellRangeAddress(2,3,4,4));
        row3.createCell(4).setCellValue("性别");

        sheet.addMergedRegion(new CellRangeAddress(2,3,5,5));
        row3.createCell(5).setCellValue("毕业学校");

        sheet.addMergedRegion(new CellRangeAddress(2,3,6,6));
        row3.createCell(6).setCellValue("毕业专业");

        sheet.addMergedRegion(new CellRangeAddress(2,3,7,7));
        row3.createCell(7).setCellValue("综合成绩/综合排名/排名总人数");

        sheet.addMergedRegion(new CellRangeAddress(2,3,8,8));
        row3.createCell(8).setCellValue("外语成绩");

        sheet.addMergedRegion(new CellRangeAddress(2,2,9,14));
        row3.createCell(9).setCellValue("复试成绩");

        sheet.addMergedRegion(new CellRangeAddress(2,3,15,15));
        row3.createCell(15).setCellValue("录取排名");

        sheet.addMergedRegion(new CellRangeAddress(2,3,16,16));
        row3.createCell(16).setCellValue("录取意见");

        sheet.addMergedRegion(new CellRangeAddress(2,3,17,17));
        row3.createCell(17).setCellValue("备注");

        for (int k = 10; k <= 14; k++) {
            row3.createCell(k).setCellValue("");
        }

        for (int k = 0; k <= 17; k++) {
            if(k==9) {
                row3.getCell(k).setCellStyle(Style2(wb,"BOLD","CENTER","BK"));
            }else {
                row3.getCell(k).setCellStyle(Style2(wb,"BOLD","LEFT","BK"));
            }
        }

        //在sheet里创建第四行
        HSSFRow row4=sheet.createRow(3);
        //创建单元格并设置单元格内容
        for (int j = 0; j <9; j++) {
            row4.createCell(j).setCellValue("");
        }

        row4.createCell(9).setCellValue("复试科目1");
        row4.createCell(10).setCellValue("复试科目2");
        row4.createCell(11).setCellValue("复试科目3");
        row4.createCell(12).setCellValue("复试科目4");
        row4.createCell(13).setCellValue("复试科目5");
        row4.createCell(14).setCellValue("复试总成绩");

        for (int j = 15; j <18; j++) {
            row4.createCell(j).setCellValue("");
        }
        for (int k = 0; k <= 17; k++) {
            row4.getCell(k).setCellStyle(Style2(wb,"BOLD","LEFT","BK"));
        }

        HSSFCellStyle cellStyle = Style2(wb,"","LEFT","BK");
        cellStyle.setWrapText(true);
        for (int i = 4; i <100; i++) {
            HSSFRow row5 = sheet.createRow(i);
            row5.createCell(0).setCellValue((i+1));
            for (int j = 1; j <= 17; j++){
                Cell cellContent = row5.createCell(j);
                cellContent.setCellStyle(cellStyle);
                cellContent.setCellValue("中国人民解放军");
//                row5.createCell(j).setCellValue("中国人民解放军");
            }

            row5.setHeight((short)1800);

            /*for (int k = 0; k <= 17; k++) {
                row5.getCell(k).setCellStyle(cellStyle);
            }*/
        }

        String filename = "create_excel_20210923_11111111111111111.xlsx";
        File outFile = new File("D://home//" + filename);
        FileOutputStream out = new FileOutputStream(outFile);
        wb.write(out);
        out.close();

        String sourceFilePath="d:/home/" + filename;
        String desFilePath="d:/home/" + filename + ".pdf";
        PdfUtils.excel2pdf(sourceFilePath, desFilePath);

    }


    //typeBOLD字体    typeALIGN：靠右、左、居中     BK:加边框
    public static HSSFCellStyle Style2(HSSFWorkbook wb, String typeBOLD,String typeALIGN,String BK) {
        // 普通单元格样式
        HSSFCellStyle style = wb.createCellStyle();
        if(typeALIGN.equals("RIGHT")) {
            style.setAlignment(HorizontalAlignment.RIGHT);
        } else if (typeALIGN.equals("LEFT")) {
            style.setAlignment(HorizontalAlignment.LEFT);
        } else {
//            style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 左右居中
            style.setAlignment(HorizontalAlignment.CENTER);
        }

//        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);// 上下居中
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setLocked(true);
        style.setWrapText(true);// 自动换行
        if(typeBOLD.equals("BOLD")) {
            Font font = wb.createFont();
            font.setBold(true);
            font.setFontHeight((short)240);
            font.setFontName("黑体");
            style.setFont(font);
        }
        if(BK.equals("BK")) {
            style.setBorderBottom(BorderStyle.THIN);
            style.setBorderLeft(BorderStyle.THIN);
            style.setBorderRight(BorderStyle.THIN);
            style.setBorderTop(BorderStyle.THIN);
            /*style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);*/
        }

        return style;
    }

    public static short getRowHeight(int width, String... content) {
        short line = 400;
        short height = 1000;
        return height;
    }

}
