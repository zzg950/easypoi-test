package cn.afterturn.easypoi.zzgdemo.controller;

import cn.afterturn.easypoi.util.WebFilenameUtils;
import cn.afterturn.easypoi.zzgdemo.utils.PdfUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.OutputStream;

/**
 * @Author 张志刚
 * @Date 2021/9/24
 * @Description TODO
 *
 * Linux服务器导出PDF乱码解决办法
 * 原因：缺少字体文件
 * 解决：将window服务器的字体安装到服务器上
 * 参考：https://www.jianshu.com/p/d78b37d33779
 *      https://developer.aliyun.com/article/109936
 *
 */
@Controller
@RequestMapping("aspose")
public class AsposeController {

    /**
     * 将Word转成PDF然后下载
     * @param response
     * @throws Exception
     */
    @RequestMapping("word2pdf")
    public void word2pdfAndDownload(String filepath, HttpServletResponse response) throws Exception {
        String wordPath = filepath;
        String pdfPath = filepath + ".pdf";
        PdfUtils.word2pdf(wordPath, pdfPath);
        response.addHeader( "Content-Type", "application/octet-stream");
        response.setHeader("content-disposition", WebFilenameUtils.disposition("Word转PDF下载的.pdf"));
        try(FileInputStream input = new FileInputStream(pdfPath);
            OutputStream out = response.getOutputStream()) {
            byte[] bytes = new byte[1024];
            int len;
            while ((len = input.read(bytes)) != -1) {
                out.write(bytes, 0, len);
            }
        }
    }

    /**
     * 将Excel转成PDF然后下载
     * @param response
     * @throws Exception
     */
    @RequestMapping("excel2pdf")
    public void excel2pdfAndDownload(String filepath, HttpServletResponse response) throws Exception {
        String excelPath = filepath;
        String pdfPath = filepath + ".pdf";
        PdfUtils.excel2pdf(excelPath, pdfPath);
        response.addHeader( "Content-Type", "application/octet-stream");
        response.setHeader("content-disposition", WebFilenameUtils.disposition("Excel转PDF下载的.pdf"));
        try(FileInputStream input = new FileInputStream(pdfPath);
            OutputStream out = response.getOutputStream()) {
            byte[] bytes = new byte[1024];
            int len;
            while ((len = input.read(bytes)) != -1) {
                out.write(bytes, 0, len);
            }
        }
    }

}
