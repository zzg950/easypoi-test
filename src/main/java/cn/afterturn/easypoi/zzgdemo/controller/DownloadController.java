package cn.afterturn.easypoi.zzgdemo.controller;

import cn.afterturn.easypoi.util.WebFilenameUtils;
import cn.afterturn.easypoi.zzgdemo.utils.PdfUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author 张志刚
 * @Date 2021/10/20
 * @Description TODO
 */
@Controller
@RequestMapping("download")
public class DownloadController {

    /**
     * 直接将服务器文件下载到本地
     */
    @RequestMapping("downloadOne")
    public void downloadOne(HttpServletResponse response) throws Exception {
        String filePath = "F:\\Download\\Xshell6_onlinedown.exe";
        File file = new File(filePath);
        response.addHeader( "Content-Type", "application/octet-stream");
        response.setHeader("content-disposition", WebFilenameUtils.disposition(file.getName()));
        try(FileInputStream input = new FileInputStream(file);
            OutputStream out = response.getOutputStream()) {
            byte[] bytes = new byte[1024];
            int len;
            while ((len = input.read(bytes)) != -1) {
                out.write(bytes, 0, len);
            }
        }
    }


    /**
     * https://blog.csdn.net/weixin_39608116/article/details/114551992
     * 直接将服务器文件下载到本地
     * 想一次下载多个 - 测试行不通
     */
    @RequestMapping("downloadMore2")
    public void downloadMore2(HttpServletResponse response) throws Exception {
        List<String> fileList = new ArrayList<>();
        fileList.add("F:\\BaiduNetdiskDownload\\adobe acrobat X pro.zip");
        fileList.add("F:\\BaiduNetdiskDownload\\Navicat_Premium_10_中文版.exe");
        response.setContentType("multipart/x-mixed-replace;boundary=END");
        ServletOutputStream out = response.getOutputStream();
        out.println("--END");
        for (String filePath : fileList) {
            FileInputStream fis = new FileInputStream(filePath);
            BufferedInputStream fif = new BufferedInputStream(fis);
            int data = 0;
            out.println("--END");
            while ((data = fif.read()) != -1) {
                out.write(data);
            }
            fif.close();
            out.println("--END");
            out.flush();
        }

        out.flush();
        out.println("--END--");
        out.close();
    }

    /**
     * 直接将服务器文件下载到本地
     * 想一次下载多个 - 测试行不通
     */
    @RequestMapping("downloadMore")
    public void downloadMore(HttpServletResponse response) throws Exception {
        List<String> fileList = new ArrayList<>();
        fileList.add("F:\\BaiduNetdiskDownload\\Navicat_Premium_10_中文版.exe");
        fileList.add("F:\\BaiduNetdiskDownload\\adobe acrobat X pro.zip");
        OutputStream out = response.getOutputStream();
        for (String filePath : fileList) {
            File file = new File(filePath);
            response.addHeader( "Content-Type", "application/octet-stream");
            response.setHeader("content-disposition", WebFilenameUtils.disposition(file.getName()));
            try(FileInputStream input = new FileInputStream(file)) {
                byte[] bytes = new byte[1024];
                int len;
                while ((len = input.read(bytes)) != -1) {
                    out.write(bytes, 0, len);
                }
            }
        }
        out.flush();
        out.close();
    }
}
