package cn.afterturn.easypoi.zzgdemo.controller;

import cn.afterturn.easypoi.entity.vo.NormalExcelConstants;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelXorHtmlUtil;
import cn.afterturn.easypoi.excel.entity.ExcelToHtmlParams;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.afterturn.easypoi.util.WebFilenameUtils;
import cn.afterturn.easypoi.view.PoiBaseView;
import cn.afterturn.easypoi.zzgdemo.model.StudentModel;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.List;

/**
 * @Author 张志刚
 * @Date 2021/9/19
 * @Description TODO
 */
@Controller
@RequestMapping("excel")
public class ExcelExportController {

    // HTML 格式预览
    @RequestMapping("review")
    public void toHtmlOf03Base(HttpServletResponse response) throws Exception {
        response.setContentType("text/html; charset=utf-8");

        List<StudentModel> list = StudentModel.getStudentList(1000);
        ExportParams exportParams = new ExportParams();
        exportParams.setTitle("计算机一班学生");
        exportParams.setSheetName("学生信息");
        Workbook workbook = ExcelExportUtil.exportExcel(exportParams, StudentModel.class, list);

        ExcelToHtmlParams htmlParams = new ExcelToHtmlParams(workbook);
        response.getOutputStream().write(ExcelXorHtmlUtil.excelToHtml(htmlParams).getBytes());
    }

    // EXCEL 文件下载
    @RequestMapping("load")
    public void load(ModelMap map, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<StudentModel> list = StudentModel.getStudentList(1000);

        // ExcelType.XSSF = .xlsx   ExcelType.HSSF = .xls
        ExportParams exportParams = new ExportParams("计算机一班学生", "学生信息", ExcelType.XSSF);
        exportParams.setFreezeCol(2);

        map.put(NormalExcelConstants.DATA_LIST, list);
        map.put(NormalExcelConstants.CLASS, StudentModel.class);
        map.put(NormalExcelConstants.PARAMS, exportParams);
        map.put(NormalExcelConstants.FILE_NAME, "这是一个excel");   // 默认叫做：临时文件

        PoiBaseView.render(map, request, response, NormalExcelConstants.EASYPOI_EXCEL_VIEW);
    }


    /**
     * 参考： 设置response
     * https://blog.csdn.net/woacptp/article/details/80199713
     */
    // EXCEL 文件下载
    @RequestMapping("load2")
    public void load2(HttpServletResponse response) throws Exception {
        List<StudentModel> list = StudentModel.getStudentList(1000);

        // ExcelType.XSSF = .xlsx   ExcelType.HSSF = .xls
        ExportParams exportParams = new ExportParams("计算机一班学生", "学生信息", ExcelType.XSSF);
        exportParams.setFreezeCol(2);

        Workbook workbook = ExcelExportUtil.exportExcel(exportParams, StudentModel.class, list);
        String filename = "这是一个EXCEL文件.xlsx";
        filename = new String(filename.getBytes( "UTF-8"), "ISO-8859-1");

//        response.setContentType("application/octet-stream");  // 效果同下面一行
        response.addHeader( "Content-Type", "application/octet-stream");
        response.addHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"; filename*=utf-8''" + filename);
        workbook.write(response.getOutputStream());
    }

}
