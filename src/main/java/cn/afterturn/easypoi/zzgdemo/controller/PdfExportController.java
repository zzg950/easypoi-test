package cn.afterturn.easypoi.zzgdemo.controller;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.pdf.PdfExportUtil;
import cn.afterturn.easypoi.pdf.entity.PdfExportParams;
import cn.afterturn.easypoi.util.WebFilenameUtils;
import cn.afterturn.easypoi.word.WordExportUtil;
import cn.afterturn.easypoi.zzgdemo.model.StudentModel;
import cn.afterturn.easypoi.zzgdemo.utils.DateUtils;
import cn.afterturn.easypoi.zzgdemo.utils.PdfUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @Author 张志刚
 * @Date 2021/9/19
 * @Description TODO
 */
@Controller
@RequestMapping("pdf")
public class PdfExportController {

    // PDF 文件下载 - Excel数据格式
    @RequestMapping("load")
    public void load(ModelMap map, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<StudentModel> list = StudentModel.getStudentList(10);

        PdfExportParams params = new PdfExportParams("这是一个一级标题","");

        String filename = "这是一个PDF文件.pdf";
        response.addHeader( "Content-Type", "application/octet-stream");
        response.setHeader("content-disposition", WebFilenameUtils.disposition(filename));

        PdfExportUtil.exportPdf(params, StudentModel.class, list, response.getOutputStream());
    }

    // PDF 文件下载 - 以模板方式生成文件
    @RequestMapping("load/template")
    public void loadByTemplate(HttpServletResponse response) throws Exception {
        Map<String, Object> data = new HashMap<>();
        data.put("title", "这是一个大标题");
        data.put("person", "JueYue");
        data.put("time", DateUtils.getDatetime());

        String filename = "生成一个PDF文件ByTemplate.pdf";
        response.addHeader( "Content-Type", "application/octet-stream");
        response.setHeader("content-disposition", WebFilenameUtils.disposition(filename));
        PdfExportUtil.exportPdf("pdf/simpleTemplate.pdf", data, response.getOutputStream());
    }


    // 内容先生成Excel，然后再转换成PDF导出
    @RequestMapping("load/byexcel")
    public void createExcelAndTransferPdfAndDownload(HttpServletResponse response) throws Exception {
        List<StudentModel> list = StudentModel.getStudentList(1000);
        ExportParams params = new ExportParams();
        params.setTitle("计算机一班学生");
        params.setHeaderColor(HSSFColor.HSSFColorPredefined.RED.getIndex());
        params.setHeaderHeight(50);
        params.setSheetName("学生信息");

        String filename = UUID.randomUUID().toString().replaceAll("-", "");
        String filepath = "D:\\" + filename + ".xlsx";
        Workbook workbook = ExcelExportUtil.exportExcel(params, StudentModel.class, list);
        FileOutputStream fos = new FileOutputStream(filepath);
        workbook.write(fos);
        fos.close();

        String desFilepath = "D:\\" + filename + ".pdf";
        PdfUtils.excel2pdf(filepath, desFilepath);

        response.addHeader( "Content-Type", "application/octet-stream");
        response.setHeader("content-disposition", WebFilenameUtils.disposition("Excel转PDF下载的.pdf"));
        File file = new File(desFilepath);
        try(FileInputStream input = new FileInputStream(file);
            OutputStream out = response.getOutputStream()) {

            byte[] bytes = new byte[1024];
            int len;
            while ((len = input.read(bytes)) != -1) {
                out.write(bytes, 0, len);
            }
        }
    }
}
