package cn.afterturn.easypoi.zzgdemo.controller;

import cn.afterturn.easypoi.entity.vo.NormalExcelConstants;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.afterturn.easypoi.util.WebFilenameUtils;
import cn.afterturn.easypoi.view.EasypoiTemplateWordView;
import cn.afterturn.easypoi.view.PoiBaseView;
import cn.afterturn.easypoi.word.WordExportUtil;
import cn.afterturn.easypoi.zzgdemo.model.StudentModel;
import cn.afterturn.easypoi.zzgdemo.utils.DateUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author 张志刚
 * @Date 2021/9/19
 * @Description TODO
 */
@Controller
@RequestMapping("word")
public class WordExportController {

    // Word 文件下载
    @RequestMapping("load")
    public void load(HttpServletResponse response) throws Exception {
        Map<String, Object> data = new HashMap<>();
        data.put("title", "这是一个大标题");
        data.put("person", "JueYue");
        data.put("time", DateUtils.getDatetime());

        XWPFDocument document = WordExportUtil.exportWord07("word/simpleTemplate.docx", data);

        String filename = "这是一个Word文件.docx";
        response.addHeader( "Content-Type", "application/octet-stream");
        response.setHeader("content-disposition", WebFilenameUtils.disposition(filename));
        document.write(response.getOutputStream());
    }


    /**
     * 多页word文档导出 - 每一页的格式一样
     */
    @RequestMapping("load/more/page")
    public static void exportLocalMorePageFile(HttpServletResponse response) throws Exception {
        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("title", "标题" + i);
            map.put("name", "姓名" + i);
            map.put("birthday", DateUtils.getDate());
            list.add(map);
        }

        XWPFDocument document = WordExportUtil.exportWord07("word/morePageTemplate.docx", list);
        String filename = "这是一个Word文件.docx";
        response.addHeader( "Content-Type", "application/octet-stream");
        response.setHeader("content-disposition", WebFilenameUtils.disposition(filename));
        document.write(response.getOutputStream());
    }

}
