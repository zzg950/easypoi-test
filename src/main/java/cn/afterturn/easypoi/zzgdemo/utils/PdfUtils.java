package cn.afterturn.easypoi.zzgdemo.utils;

import com.aspose.cells.PdfSaveOptions;
import com.aspose.cells.Workbook;
import com.aspose.words.Document;
import com.aspose.words.SaveFormat;

import java.io.*;

/**
 * @Author 张志刚
 * @Date 2021/9/22
 * @Description TODO
 * 注意： Word转PDF和Excel转PDF，所使用的License对象不同
 */
public class PdfUtils {

    /**
     * 依赖包：com.aspose.aspose-words
     * word 转 pdf 输出
     * @param docPath
     * @param pdfPath
     */
    public static void word2pdf(String docPath,String pdfPath) {
        if (!getWordLicense()) {
            System.out.println("========word2pdf=====License 验证有问题==========");
            return;
        }
        try {
            long start = System.currentTimeMillis();
            Document convertDoc = new Document(new FileInputStream(docPath));
            convertDoc.save(pdfPath, SaveFormat.PDF);
            long now = System.currentTimeMillis();
            System.out.println("Word转PDF完毕======成功=====耗时： " + ((now - start) / 1000.0) + "秒");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 依赖包：com.aspose.aspose-words
     * word 转 pdf 输出
     * @param sourceFilePath
     * @param desFilePathd
     */
    public static void word2pdfV2(String sourceFilePath, String desFilePathd) {
        if (!getWordLicense()) {
            System.out.println("========word2pdf=====License 验证有问题==========");
            return;
        }

        try {
            long start = System.currentTimeMillis();
            Document doc = new Document(sourceFilePath); //sourceFilePath是将要被转化的word文档
            doc.save(desFilePathd, com.aspose.words.SaveFormat.PDF);//全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
            long now = System.currentTimeMillis();
            System.out.println("Word转PDF完毕======成功=====耗时： " + ((now - start) / 1000.0) + "秒");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 依赖包：com.aspose.aspose-cells
     * excel 转为pdf 输出。
     * @param sourceFilePath  excel文件
     * @param desFilePathd  pad 输出文件目录
     */
    public static void excel2pdf(String sourceFilePath, String desFilePathd){
        // 验证License 若不验证则转化出的pdf文档会有水印产生
        if (!getCellLicense()) {
            System.out.println("========excel2pdf=====License 验证有问题==========");
            return;
        }
        try {
            long start = System.currentTimeMillis();
            Workbook wb = new Workbook(sourceFilePath);// 原始excel路径
            FileOutputStream fileOS = new FileOutputStream(desFilePathd);
            PdfSaveOptions pdfSaveOptions = new PdfSaveOptions();
            pdfSaveOptions.setOnePagePerSheet(true);

            //当excel中对应的sheet页宽度太大时，在PDF中会拆断并分页。此处等比缩放。
//            int[] autoDrawSheets={3};
//            autoDraw(wb,autoDrawSheets);
            int[] showSheets={0};
            //隐藏workbook中不需要的sheet页。
            printSheetPage(wb, showSheets);
            wb.save(fileOS, pdfSaveOptions);
            fileOS.flush();
            fileOS.close();
            long end = System.currentTimeMillis();
            System.out.println("Excel转PDF完毕======成功=====耗时： " + ((end - start) / 1000.0) + "秒");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取license去除水印
     * @return
     */
    public static boolean getCellLicense() {
        boolean result = false;
        try {
            InputStream is = PdfUtils.class.getClassLoader().getResourceAsStream("\\license.xml");
            com.aspose.cells.License asposeLic = new com.aspose.cells.License();
            asposeLic.setLicense(is);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 获取license去除水印
     * @return
     */
    public static boolean getWordLicense() {
        boolean result = false;
        try {
            InputStream is = PdfUtils.class.getClassLoader().getResourceAsStream("\\license.xml");
            com.aspose.words.License asposeLic = new com.aspose.words.License();
            asposeLic.setLicense(is);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * word license 去除水印
     */
    public static boolean getWordLicenseV2() {
        boolean result = false;
        try {
            // 凭证
            String license =
            "<License>\n" +
            "  <Data>\n" +
            "    <Products>\n" +
            "      <Product>Aspose.Total for Java</Product>\n" +
            "      <Product>Aspose.Words for Java</Product>\n" +
            "    </Products>\n" +
            "    <EditionType>Enterprise</EditionType>\n" +
            "    <SubscriptionExpiry>20991231</SubscriptionExpiry>\n" +
            "    <LicenseExpiry>20991231</LicenseExpiry>\n" +
            "    <SerialNumber>8bfe198c-7f0c-4ef8-8ff0-acc3237bf0d7</SerialNumber>\n" +
            "  </Data>\n" +
            "  <Signature>sNLLKGMUdF0r8O1kKilWAGdgfs2BvJb/2Xp8p5iuDVfZXmhppo+d0Ran1P9TKdjV4ABwAgKXxJ3jcQTqE/2IRfqwnPf8itN8aFZlV3TJPYeD3yWE7IT55Gz6EijUpC7aKeoohTb4w2fpox58wWoF3SNp6sK6jDfiAUGEHYJ9pjU=</Signature>\n" +
            "</License>";
            InputStream is = new ByteArrayInputStream(license.getBytes("UTF-8"));
            com.aspose.words.License asposeLic = new com.aspose.words.License();
            asposeLic.setLicense(is);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 设置打印的sheet 自动拉伸比例
     * @param wb
     * @param page 自动拉伸的页的sheet数组
     */
    private static void autoDraw(Workbook wb,int[] page){
        if(null!=page&&page.length>0){
            for (int i = 0; i < page.length; i++) {
                wb.getWorksheets().get(i).getHorizontalPageBreaks().clear();
                wb.getWorksheets().get(i).getVerticalPageBreaks().clear();
            }
        }
    }

    /**
     * 隐藏workbook中不需要的sheet页。
     * @param wb
     * @param page 显示页的sheet数组
     */
    private static void printSheetPage(Workbook wb, int[] page){
        for (int i= 1; i < wb.getWorksheets().getCount(); i++)  {
            wb.getWorksheets().get(i).setVisible(false);
        }
        if(null==page||page.length==0){
            wb.getWorksheets().get(0).setVisible(true);
        }else{
            for (int i = 0; i < page.length; i++) {
                wb.getWorksheets().get(i).setVisible(true);
            }
        }
    }

}
